.DEFAULT_GOAL := help
help:
	@grep -E '(^[1-9a-zA-Z_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}{printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'
.PHONY: help

## -------------
## Images
## -------------

build: ## Builds the docker image
	./docker/build.sh "games/cross"


## -------------
## Tools
## -------------

install: ## Installs required dependencies
	docker-compose -p client -f ./docker/docker-compose.yml run --rm client \
	npm install

run: ## Runs the application
	docker-compose -p client -f ./docker/docker-compose.yml run --rm client \
	npm start

exec: ## Executes a custom command into the container
	docker-compose -p client -f ./docker/docker-compose.yml run --rm client \
	$(CMD)

