import { NB_DIRECTIONS } from './constants.js';

export const getBit = (number, bitIndex) => {
  return (number & (1 << bitIndex)) === 0 ? 0 : 1;
};

export const setBitTrue = (number, bitIndex) => {
  return number | (1 << bitIndex);
};

export const setBitFalse = (number, bitIndex) => {
  const mask = ~(1 << bitIndex);
  return number & mask;
};

export const setBit = (number, bitIndex, bitValue) => {
  return bitValue ? setBitTrue(number, bitIndex) : setBitFalse(number, bitIndex);
};

export const getOppositeIndex = (bitIndex) => {
  const halfNbOfDirections = Math.round(NB_DIRECTIONS / 2);

  return bitIndex >= halfNbOfDirections
    ? bitIndex - halfNbOfDirections
    : bitIndex + halfNbOfDirections;
};
