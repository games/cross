export const fillWithMap = (length, callback) => Array.from({ length }, callback);

export const mapToArray = (map) => Array.from(map).map(([, item]) => item);

export const getRandomItem = (array) => array[Math.floor(Math.random() * array.length)];

export const shuffle = (array) => {
  for (let i = array.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [array[i], array[j]] = [array[j], array[i]];
  }
  return array;
};
