import { DEBUG_ENABLED, DISPLAY_COORDINATES } from './constants.js';

export const debug = (...args) => {
  if (isDebugEnabled()) {
    console.debug(...args);
  }
};

export const isDebugEnabled = () => {
  return DEBUG_ENABLED;
};

export const displayBoardCoordinates = () => {
  return DEBUG_ENABLED && DISPLAY_COORDINATES;
};
