/**
 * Return coordinates as string.
 */
export const xy = ({ x, y }) => {
  return `${x}-${y}`;
};

export const getCloseCoordinates = (nb, { x, y }, bitDirection, bounds) => {
  const coordinates = [];

  const xModifier = getXModifier(bitDirection);
  const yModifier = getYModifier(bitDirection);

  for (let i = 1, nextX = 0, nextY = 0; i <= nb && !isOutOfBounds(nextX, nextY, bounds); i++) {
    nextX = x + i * xModifier;
    nextY = y + i * yModifier;
    coordinates.push({ x: nextX, y: nextY });
  }

  return coordinates;
};

const isOutOfBounds = (x, y, { minX, maxX, minY, maxY }) => {
  const isXOutOfBounds = (x) => x < minX || x > maxX;
  const isYOutOfBounds = (y) => y < minY || y > maxY;

  return isXOutOfBounds(x) || isYOutOfBounds(y);
};

const getXModifier = (bitDirection) => {
  if ([0, 1, 7].includes(bitDirection)) {
    return 1;
  }

  if ([3, 4, 5].includes(bitDirection)) {
    return -1;
  }

  return 0;
};

const getYModifier = (bitDirection) => {
  if ([1, 2, 3].includes(bitDirection)) {
    return 1;
  }

  if ([5, 6, 7].includes(bitDirection)) {
    return -1;
  }

  return 0;
};
