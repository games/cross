import { readFileSync, writeFileSync } from 'fs';
import { DATA_DIRECTORY_PATH } from './constants.js';
import { format } from 'date-fns';

export const getFileContent = (fileName) => {
  const filePathName = `${DATA_DIRECTORY_PATH}/${fileName}`;
  const fileContent = readFileSync(filePathName);

  try {
    return JSON.parse(fileContent);
  } catch (error) {
    return fileContent;
  }
};

export const writeToFile = (data) => {
  const dataToWrite = typeof data === 'string'
    ? data
    : JSON.stringify(data);

  const filePathName = `${DATA_DIRECTORY_PATH}/${generateFileName()}.json`;

  writeFileSync(filePathName, dataToWrite);
};


const generateFileName = () => {
  return format(new Date(), 'yyyy-MM-dd_HH-mm-ss');
};
