import { displayScore, drawBoard } from './view/view.js';
import { Game, loadGameFromFile } from './domain/game.js';
import { SAVE_HISTORY_SCORE_THRESHOLD } from './util/constants.js';

// Plays a game an display the board at the end
playOneGame();

// Plays the number of games you want and display the best score
// playMultipleGames(100000);

// Loads a game from the file and display the board
// load('2021-09-04_14-59-10.json');

function playOneGame() {
  const game = new Game();
  game.playAllGameRandomly();
  drawBoard(game.board);

  if (game.getScore() > SAVE_HISTORY_SCORE_THRESHOLD) {
    game.history.save();
  }

  displayScore(game.getScore());
}

function playMultipleGames(nbOfGamesToPlay) {
  const game = new Game();

  const scores = new Set();

  for (let i = 0; i < nbOfGamesToPlay; i++) {
    game.playAllGameRandomly();

    if (game.getScore() > SAVE_HISTORY_SCORE_THRESHOLD) {
      game.history.save();
    }

    scores.add(game.getScore());
    game.init();
  }

  const maxScore = Math.max(...Array.from(scores));
  displayScore(maxScore);
}

function load(fileName) {
  const game = loadGameFromFile(fileName);
  drawBoard(game.board);
  displayScore(game.getScore());
}
