import { displayBoardCoordinates } from '../util/debug.js';

export const displayScore = (score) => {
  console.log(`Game finished with ${score} lines!`);
};

export const drawBoard = (board) => {
  getLinesFromBottomToTop(board)
    .reverse()
    .forEach(drawLine);
};

const getLinesFromBottomToTop = (board) => {
  const { minX, maxX, minY, maxY } = board.getBounds();

  const lines = [];
  for (let y = minY; y <= maxY; y++) {
    const line = [];
    for (let x = minX; x <= maxX; x++) {
      line.push(
        board.getNode({ x, y })
      );
    }
    lines.push(line);
  }

  return lines;
};

const drawLine = (line) => {
  line
    .reduce(
      (aggregated, node) => {
        const [nodeFirstLine, nodeSecondLine, nodeThirdLine] = getNodeAsStringArray(node);

        aggregated[0].push(...nodeFirstLine);
        aggregated[1].push(...nodeSecondLine);
        aggregated[2].push(...nodeThirdLine);

        return aggregated;
      },
      [[], [], []]
    )
    .forEach(subLines => console.log(...subLines));
};

/**
 * Builds the representation of a node.
 *
 * <pre>
 * \ | /     3 2 1
 * — · —     4 · 0
 * / | \     5 6 7
 * </pre>
 *
 * @param {Node} node
 * @return {[[(string), (string), (string)], [(*|string), (string), (*|string)], [(string), (string), (string)]]}
 */
const getNodeAsStringArray = (node) => {
  const bitMap = node.toBitMap();
  const { x, y } = node.getCoordinates();

  return [
    [
      bitMap.get(3) ? '\\' : ' ',
      bitMap.get(2) ? '|' : ' ',
      bitMap.get(1) ? '/' : ' '
    ],
    [
      displayBoardCoordinates()
        ? x
        : bitMap.get(4) ? '-' : ' ',
      node.isExisting() ? '·' : ' ',
      displayBoardCoordinates()
        ? y
        : bitMap.get(0) ? '-' : ' '
    ],
    [
      bitMap.get(5) ? '/' : ' ',
      bitMap.get(6) ? '|' : ' ',
      bitMap.get(7) ? '\\' : ' '
    ]
  ];
};
