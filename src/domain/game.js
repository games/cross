import { Board } from './board.js';
import { getRandomItem, shuffle } from '../util/array.js';
import { History } from './history.js';
import { getFileContent } from '../util/file.js';
import { Vector } from './vector.js';

export class Game {
  constructor() {
    this.init();
  }

  init() {
    this.history = new History();
    this.board = new Board();
    this.isFinished = false;
    this.nbOfLines = 0;
  }

  getScore() {
    return this.nbOfLines;
  }

  play(vector) {
    if (this.isFinished) {
      return;
    }

    // const origin = this.board.getNode({ x, y });
    // const vector = new Vector(this.board, origin, bitDirection);
    const { origin, bitDirection } = vector;
    const { x, y } = origin.getCoordinates();

    if (!vector.isValid()) {
      throw Error(`Invalid play : (${x}, ${y}) with "${bitDirection}" direction`);
    }

    vector.apply();
    this.history.savePlay(vector);
    this.nbOfLines++;
  }

  playRandomly() {
    if (this.isFinished) {
      return;
    }

    const existingNodes = shuffle(
      this.board.getExistingNodes()
    );

    let validVectorFound = false;
    for (let i = 0, nbOfNodes = existingNodes.length; i < nbOfNodes && !validVectorFound; i++) {
      const node = existingNodes[i];
      const randomVector = getRandomItem(this.board.getValidVectors(node));

      try {
        this.play(randomVector);
      } catch (invalidPlayError) {
        continue;
      }

      validVectorFound = true;
    }

    this.isFinished = !validVectorFound;
  }

  playRandomlyMultipleTimes(nbOfTimes) {
    if (!this.isFinished && nbOfTimes > 0) {
      this.playRandomly();
      this.playRandomlyMultipleTimes(nbOfTimes - 1);
    }
  }

  playAllGameRandomly() {
    while (!this.isFinished) {
      this.playRandomly();
    }
  }
}

export const loadGameFromFile = (fileName) => {
  const gameToLoad = getFileContent(fileName);
  if (!gameToLoad?.plays?.length) {
    throw Error(`Cannot read valid data from file "${fileName}"`);
  }

  const game = new Game();

  gameToLoad.plays
    .map(([x, y, bitDirection]) => {
      const origin = game.board.getNode({ x, y });

      return new Vector(game.board, origin, bitDirection);
    })
    .forEach(vector => game.play(vector));

  // gameToLoad.plays.forEach(([x, y, bitDirection]) => {
  //   const origin = game.board.getNode({ x, y });
  //
  //   game.play(
  //     // Vectors must be instantiated sequentially
  //     // because a vector may be invalid at one point but valid later in the game
  //     // This is because
  //     new Vector(game.board, origin, bitDirection)
  //   );
  // })

  return game;
};
