import { writeToFile } from '../util/file.js';

export class History {
  constructor() {
    this.plays = [];
    this.score = 0;
  }

  savePlay(vector) {
    const { origin, bitDirection } = vector;
    const { x, y } = origin.getCoordinates();

    this.plays.push([x, y, bitDirection]);

    this.score++;
  }

  save() {
    writeToFile({
      plays: this.plays,
      score: this.score
    });
  }
}
