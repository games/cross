import { getCloseCoordinates } from '../util/coordinates.js';
import { BOARD_BOUNDS, VECTOR_SIZE } from '../util/constants.js';
import { debug } from '../util/debug.js';

/**
 * All vectors have the "same" norm : the distance to rely 5 points.
 *
 * The bit indicates one of the 8 directions available from the node.
 *
 * To be valid, the node must be exists. If it's not not, the reversed vector may be used instead.
 */
export class Vector {
  constructor(board, origin, bitDirection) {
    this.board = board;
    this.origin = origin;
    this.bitDirection = bitDirection;
    this.nodesToBindWithOrigin = undefined;
  }

  isValid() {
    const nodesToBindWithOrigin = this.getNodes();
    if (nodesToBindWithOrigin.length === 0) {
      return false;
    }

    if (!this.origin.isExisting()) {
      return false;
    }

    const existingNodes = nodesToBindWithOrigin.filter((node) => node.isExisting());
    const isValidNbOfExistingNodes = existingNodes.length >= nodesToBindWithOrigin.length - 1;

    return isValidNbOfExistingNodes
      && !this.origin.isUsedTowards(this.bitDirection)
      && !nodesToBindWithOrigin.some((node) => node.isUsedFrom(this.bitDirection));
  }

  apply() {
    const nodesToBindWithOrigin = this.getNodes();
    nodesToBindWithOrigin.forEach((node, index) => {
      const isLast = index === nodesToBindWithOrigin.length - 1;
      node.updateBits(this.bitDirection, true, !isLast);

      this.board.addExistingNode(node);
    });

    this.origin.updateBits(this.bitDirection, false, true);

    debug('Vector applied', this.origin.getCoordinates(), this.bitDirection);
  }

  getNodes() {
    if (typeof this.nodesToBindWithOrigin === 'undefined') {
      const nodeCoordinates = this.origin.getCoordinates();

      const closeCoordinates = getCloseCoordinates(VECTOR_SIZE - 1, nodeCoordinates, this.bitDirection, BOARD_BOUNDS);
      if (closeCoordinates.length < VECTOR_SIZE - 1) {
        return [];
      }

      this.nodesToBindWithOrigin = closeCoordinates.map(coordinates => this.board.getNode(coordinates));
    }

    return this.nodesToBindWithOrigin;
  }
}
