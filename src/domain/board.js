import { getEmptyNode } from './node.js';
import { Vector } from './vector.js';
import { BOARD_BOUNDS, BOARD_SIZE, DIRECTIONS, MIN_BOARD_SIZE } from '../util/constants.js';
import { xy } from '../util/coordinates.js';
import { mapToArray } from '../util/array.js';

export class Board {
  constructor() {
    checkBoardDimensions();

    this.nodes = getInitialNodes();
    this.existingNodes = calcExistingNodes(this.nodes);
  }

  getNode({ x, y }) {
    if (!this.nodes.has(xy({ x, y }))) {
      throw Error(`Invalid coordinates : (${x}, ${y})!`);
    }

    return this.nodes.get(xy({ x, y }));
  }

  /**
   * @return {Node[]}
   */
  getExistingNodes() {
    return mapToArray(this.existingNodes);
  }

  addExistingNode(node) {
    this.existingNodes.set(
      xy(node.getCoordinates()),
      node
    );
  }

  getPossibleVectors(node) {
    return DIRECTIONS.map(bitDirection => new Vector(this, node, bitDirection));
  }

  getValidVectors(node) {
    return this.getPossibleVectors(node)
      .filter((vector) => vector.isValid());
  }

  getBounds() {
    return BOARD_BOUNDS;
  }
}

const checkBoardDimensions = () => {
  const areEvenBoardDimensions = BOARD_SIZE.horizontal % 2 === 0
    && BOARD_SIZE.vertical % 2 === 0;
  if (!areEvenBoardDimensions) {
    throw Error(`Invalid board dimensions (${BOARD_SIZE.horizontal}, ${BOARD_SIZE.vertical}). Both must be even numbers.`);
  }

  if (BOARD_SIZE.vertical < 10 || BOARD_SIZE.horizontal < 10) {
    throw Error(`Invalid board dimensions (${BOARD_SIZE.horizontal}, ${BOARD_SIZE.vertical}). Minimal board is 10x10.`);
  }
};

const getInitialNodes = () => {
  const allNodes = getAllBoardNodes();

  return setCrossDefaultPosition(allNodes);
};

const getAllBoardNodes = () => {
  const nodes = new Map();

  for (let x = 0; x < BOARD_SIZE.horizontal; x++) {
    for (let y = 0; y < BOARD_SIZE.horizontal; y++) {
      nodes.set(xy({ x, y }), getEmptyNode({ x, y }));
    }
  }

  return nodes;
};

/**
 * Updates the node map to build the cross default position.
 *
 * @param {Map<String, Node>} nodes
 */
const setCrossDefaultPosition = (nodes) => {
  // Coordinates of all nodes existing at the beginning, on a minimal sized board
  // (line by line from the bottom to the top)
  const coordinatesOfNodesExistingAtBeginning = [
    [3, 0], [4, 0], [5, 0], [6, 0],
    [3, 1], [6, 1],
    [3, 2], [6, 2],
    [0, 3], [1, 3], [2, 3], [3, 3], [6, 3], [7, 3], [8, 3], [9, 3],
    [0, 4], [9, 4],
    [0, 5], [9, 5],
    [0, 6], [1, 6], [2, 6], [3, 6], [6, 6], [7, 6], [8, 6], [9, 6],
    [3, 7], [6, 7],
    [3, 8], [6, 8],
    [3, 9], [4, 9], [5, 9], [6, 9]
  ];

  const verticalPadding = (BOARD_SIZE.vertical - MIN_BOARD_SIZE.vertical) / 2;
  const horizontalPadding = (BOARD_SIZE.horizontal - MIN_BOARD_SIZE.horizontal) / 2;

  coordinatesOfNodesExistingAtBeginning
    .map(([x, y]) => ({ x: x + horizontalPadding, y: y + verticalPadding }))
    .map((coordinates) => nodes.get(xy(coordinates)))
    .forEach(node => {
      node.setExistingTrue();
    });

  return nodes;
};

/**
 * Build the map of existing nodes from all given nodes.
 *
 * @param {Map<string, Node>} allNodes
 * @return {Map<string, Node>}
 */
const calcExistingNodes = (allNodes) => {
  return new Map(
    Array.from(allNodes)
      .map(([, node]) => node)
      .filter(node => node.isExisting())
      .map(node => [xy(node.getCoordinates()), node])
  );
};
