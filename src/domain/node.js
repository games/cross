import { getBit, getOppositeIndex, setBitTrue } from '../util/bits.js';
import { fillWithMap } from '../util/array.js';

/**
 * A node is represented by 8 bits.
 *
 * Each line that can start or cross the node is bound to a bit.
 * The first bit is for the horizontal middle right line, and next follows the anti-clockwise.
 * Since a full cross node is like an eight pointed star, there is 8² possibilities : 64.
 *
 * The node can be drawn as a 3x3 map, with 8 bits around the center :
 * <pre>
 * 3 2 1
 * 4 · 0
 * 5 6 7
 * </pre>
 *
 * A node exists if it is present on the board at the beginning of the game, or after being added with a new line.
 */
export class Node {
  constructor({ x, y }, bits, exists = false) {
    this.x = x;
    this.y = y;
    this.bits = bits;
    this.exists = exists;
  }

  getCoordinates() {
    return { x: this.x, y: this.y };
  }

  setCoordinates({ x, y }) {
    this.x = x;
    this.y = y;

    return this;
  }

  isExisting() {
    return this.exists;
  }

  setExistingTrue() {
    this.exists = true;

    return this;
  }

  /**
   * If a line binds the node from the given direction.
   */
  isUsedFrom(bitDirection) {
    return !!getBit(this.bits, getOppositeIndex(bitDirection));
  }

  /**
   * If a line starts from the node, towards the given direction.
   */
  isUsedTowards(bitDirection) {
    return !!getBit(this.bits, bitDirection);
  }

  /**
   * Adds a line to the node, from and/or towards the given direction
   */
  updateBits(bitDirection, updateFrom = true, updateTowards = false) {
    if (updateFrom) {
      this.bits = setBitTrue(this.bits, getOppositeIndex(bitDirection));
    }
    if (updateTowards) {
      this.bits = setBitTrue(this.bits, bitDirection);
    }

    this.exists = true;

    return this;
  }

  toBitMap() {
    const bitMap = new Map();

    fillWithMap(8, (emptyItem, index) => {
      return getBit(this.bits, index);
    })
      .forEach((bitValue, index) => {
        bitMap.set(index, bitValue);
      });

    return bitMap;
  }
}

export const getEmptyNode = (coordinates) => {
  return new Node(coordinates, 0b0);
};
