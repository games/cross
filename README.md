# Jeu du morpion solitaire

Cette application permet de simuler le jeu du [morpion solitaire](https://fr.wikipedia.org/wiki/Morpion_solitaire).

Celui-ci consiste à ajouter une croix à 4 autres alignés, formant ainsi une ligne de 5. On la raye pour
la mémoriser et scorer un point.  
Le but est d'obtenir le plus haut score possible, sachant que la configuration initiale est toujours la même :
une grande croix grecque de 3 ou 4 de côté. (Dans cette application, c'est la version à 4 de côté qui est utilisée.)

Ex : une croix grecque de 3 de côté, avec une première ligne horizontale en haut.
```
     x-x-x-x
     x   x
 x x x   x x x
 x           x
 x x x   x x x
     x   x
     x x x
```

**Règles :**
- interdit d'ajouter plusieurs croix manquantes d'un coup pour créer une ligne (seulement une ou zéro)
- les lignes sont autorisées également en diagonale
- on peut réutiliser une croix déjà utilisée dans une ligne, mais pas une portion de ligne

L'application permet de représenter une grille de départ ou en cours de partie, 
de simuler des parties avec des coups aléatoires (mais valides), pour déterminer le meilleur score possible à ce jeu.

## Fonctionnalités

L'application permet de :

- lancer une ou plusieurs parties (jouées automatiquement par l'application)
- charger une partie sauvegardée dans un fichier

Pas de choix possible de la fonctionnalité via la commande d'exécution. Il faut modifier le
fichier `index.js` et décommenter la ligne de fonctionnalité souhaitée.

Les parties avec un score suffisamment haut sont sauvegardées dans le répertoire `data/`.

## Installation

Lancer la commande suivante :

```bash
make install
```

## Utilisation

Lancer la commande suivante :

```bash
make run
```

L'application ne gère pas d'arguments.



## Technique

### Représentation

### Nœuds

La grille de jeu est représentée par des nœuds. 

Selon les lignes qui passent par un nœud, le traversant ou non, celui-ci peut avoir 64 formes 
différentes à l'écran. Cette information est stockée sous la forme d'un nombre binaire de 8 bits.

Les différents segments de ligne pouvant être disposés autour d'un nœud de la grille sont
associés aux bits suivants :

```
\ | /     3 2 1
— · —     4 · 0
/ | \     5 6 7
```

Quelques exemples de nœuds et leur valeur binaire :

```
0b01000001        0b10001110        0b10011001        0b00000000
                  \ | /             \                     
  · —               ·               — · —               ·  
  |                   \                 \                  
```

Note : `0b` est juste un préfixe indiquant que la suite est un nombre binaire.

En plus de cette valeur, un nœud peut comporter un point (= être présent sur la grille) ou non.  
Cela permet de distinguer les points présents de base sur la grille au démarrage de la partie
de ceux qui seront "ajoutés" au fur et à mesure que les lignes seront créées.

### Coup/ligne

Chaque coup joué est représenté sous forme d'un "vecteur", assez similaire à ceux utilisés
en mathématiques.

L'**origine** du vecteur est un nœud présent sur la grille (depuis le début ou après création d'une ligne), 
et sa **direction** est représentée par un numéro de bit de direction 
(de 0 à 7, tel que présenté précédemment).

La taille du vecteur est toujours la même, dans le sens où il doit toujours relier 4 nœuds et l'origine.  
Graphiquement ce n'est toutefois pas le cas, les diagonales étant plus longues que les lignes
verticales et horizontales.

### Algorithme

Pour qu'un coup/vecteur soit valide, il doit respecter les règles suivantes :

- l'origine doit être un nœud qui existe
- parmi les 4 autres points à relier à l'origine, au moins 3 doivent exister
  (= on n'ajoute qu'un seul nœud)
- aucune portion de ligne ne commence déjà depuis l'origine dans la direction du vecteur
- aucune portion de ligne ne finit déjà vers un des 4 autres points, depuis la direction du vecteur

## Commandes utilitaires

Des commandes utilitaires sont disponibles dans le **Makefile**, dont
la liste est disponible via la commande suivante à la racine du projet :

```bash
make help
```
